Source: python-testtools
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Robert Collins <robertc@robertcollins.net>,
 Thomas Goirand <zigo@debian.org>,
 Jelmer Vernooij <jelmer@debian.org>,
Build-Depends:
 debhelper-compat (= 9),
 dh-python,
 openstack-pkg-tools,
 python-all,
 python-extras,
 python-pbr (>= 0.11),
 python-setuptools,
 python-sphinx,
 python3-all,
 python3-pbr (>= 0.11),
 python3-setuptools,
Build-Depends-Indep:
 python-extras,
 python-fixtures,
 python-linecache2,
 python-mimeparse,
 python-testresources,
 python-traceback2,
 python-twisted,
 python-unittest2 (>= 1.1.0),
 python3-extras,
 python3-fixtures,
 python3-linecache2,
 python3-mimeparse,
 python3-testresources,
 python3-traceback2,
 python3-unittest2 (>= 1.1.0),
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/python-team/packages/python-testtools.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-testtools
Homepage: https://github.com/testing-cabal/testtools

Package: python-testtools
Architecture: all
Depends:
 python-extras,
 python-linecache2,
 python-mimeparse,
 python-pbr (>= 0.11),
 python-pkg-resources,
 python-traceback2,
 python-unittest2 (>= 1.1.0),
 ${misc:Depends},
 ${python:Depends},
Provides:
 ${python:Provides},
Recommends:
 python-fixtures,
Suggests:
 python-testtools-doc,
 python-twisted,
Description: Extensions to the Python unittest library - Python 2.x
 testtools (formerly pyunit3k) is a set of extensions to the Python standard
 library's unit testing framework. These extensions have been derived from
 years of experience with unit testing in Python and come from many different
 sources. It's hoped that these extensions will make their way into the
 standard library eventually. Also included are backports from Python trunk of
 unittest features that are not otherwise available to existing unittest users.
 .
 This package contains the libraries for Python 2.x.

Package: python-testtools-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Extensions to the Python unittest library - doc
 testtools (formerly pyunit3k) is a set of extensions to the Python standard
 library's unit testing framework. These extensions have been derived from
 years of experience with unit testing in Python and come from many different
 sources. It's hoped that these extensions will make their way into the
 standard library eventually. Also included are backports from Python trunk of
 unittest features that are not otherwise available to existing unittest users.
 .
 This package contains the documentation.

Package: python3-testtools
Architecture: all
Depends:
 python3-extras,
 python3-linecache2,
 python3-mimeparse,
 python3-pbr (>= 0.11),
 python3-pkg-resources,
 python3-traceback2,
 python3-unittest2 (>= 1.1.0),
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-testtools-doc,
Provides:
 ${python:Provides},
Description: Extensions to the Python unittest library - Python 3.x
 testtools (formerly pyunit3k) is a set of extensions to the Python standard
 library's unit testing framework. These extensions have been derived from
 years of experience with unit testing in Python and come from many different
 sources. It's hoped that these extensions will make their way into the
 standard library eventually. Also included are backports from Python trunk of
 unittest features that are not otherwise available to existing unittest users.
 .
 This package contains the libraries for Python 3.x.
